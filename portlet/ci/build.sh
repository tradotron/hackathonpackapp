#!/usr/bin/env bash
#./ci/build.sh

docker build -t portlet:1.0 .

# docker login --username=iarinnovation

docker tag portlet:1.0 iarinnovation/portlet:1.0
docker push  iarinnovation/portlet:1.0

kubectl delete -f ./docker/k8s/portlet-advisor-deployment.yaml
kubectl create -f ./docker/k8s/portlet-advisor-deployment.yaml
