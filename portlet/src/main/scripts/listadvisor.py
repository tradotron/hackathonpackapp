"""
Webservice to access basic ressource Advisor

Apify JpaAdvisorRepositoryImpl


"""
import os
import logging
from logging.handlers import RotatingFileHandler

import requests
import json

from flask import Flask, request
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from flask_jsonpify import jsonify


logger = logging.getLogger()  # pylint: disable = invalid-name
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s.%(msecs)03d] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',  # pylint: disable = invalid-name
'%m-%d %H:%M:%S')
file_handler = RotatingFileHandler('listadvisor.log', 'a', 10 * 1024 * 1024, 5)  # pylint: disable = invalid-name
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

stream_handler = logging.StreamHandler()  # pylint: disable = invalid-name
stream_handler.setLevel(logging.INFO)
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

app = Flask(__name__)
api = Api(app)


@app.route("/listadvisor")
def listadvisor():
    logger.info("Calling list advisor portlet");


    #url = 'http://apiadvisor:8080/v1/advisors'

    url= os.environ.get('CB_API_URL_ADVISOR_LIST', 'CB_API_URL_ADVISOR_LIST must exist')

    r = requests.get(url)
    json_data = json.loads(r.content.decode())
    json_data = json_data ["data"]

    response = " Une portlet en python appelant du microservice ... c'est beau !!!"
    response += "   <h2>Advisors</h2>"
    response += "<table id='advisorsTable' class='table table-striped'>"
    response +=  "<thead><tr><th>Name</th><th>Specialties</th></tr></thead>"
    response += "<tbody>"

    for line in json_data:
        response += "<tr>"
        response += "<td>" + line["first_name"] + " " + line["last_name"] + "</td>"

        #url = 'http://apiadvisor:8080/v1/advisorspecialities/' + str(line["id"])
        url= os.environ.get('CB_API_URL_ADVISOR_SPECIALITIES', 'CB_API_URL_ADVISOR_SPECIALITIES must exist') + str(line["id"])
        r_specialities = requests.get(url)
        json_data_specialities = json.loads(r_specialities.content.decode())
        json_data_specialities = json_data_specialities["data"]
        response += "<td>"
        if len(json_data_specialities) == 0:
            response += "none"
        else:
            for line_specialities in json_data_specialities:
                response += line_specialities["name"] + " "

        response += "</td>"
        response += "</tr>"

    response += "</tbody>"
    response += "</table>"

    return response


@app.route("/health")
def health():
    return "I'm alive"


if __name__ == '__main__':
    logger.info("flask server starting")
    app.run(threaded=True, debug=True)
