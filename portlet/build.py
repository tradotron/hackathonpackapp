from pybuilder.core import Author, init, use_plugin

# sudo apt-get install pandoc
# sudo pip3 install https://bitbucket.org/letmaik/snakefood/get/python3-via-six.zip
#sudo pip3 install pybuilder
#sudo pip3 install pytest
#sudo pip3 install coverage
#sudo pip3 install pytest-cov
#sudo pip3 install flake8
#sudo pip3 install pybuilder_pylint_extended
#sudo pip3 install pypandoc
#sudo pip3 install unittest-xml-reporting
#sudo pip3 install mockito
#sudo pip3 install pymetrics
#sudo pip3 install snakefood
#sudo pip3 install pybuilder-pip-tools

use_plugin("python.core")
use_plugin("exec")
use_plugin('pypi:pybuilder_pytest')
use_plugin('pypi:pybuilder_pytest_coverage')
use_plugin("python.install_dependencies")
use_plugin("python.distutils")
use_plugin("python.pycharm")
use_plugin("python.flake8") # check your code syntax and provide instructions how to clean it.
use_plugin("pypi:pybuilder_pylint_extended")
#use_plugin('pypi:pybuilder_pip_tools', '==1.*') on verra plus tard si on s'en sert. pour l'instant, gestion a la main des requirements.

#use_plugin("python.pymetrics") trop vieux pour python 3 : 2010
#use_plugin("pypi:snakefood") ne supporte pas les fichiers utf8....plutot fait pour python 2. Pydeps ne trouve pas toutes les dépendances comme ta-lib.

authors = [Author('Jerome Commeau', 'jerome.commeau@yahoo.fr'), Author('Yan Liang', 'yliang012913304@gmail.com'), Author('Stephane Valon', 'stephane.valon@gmail.com'), Author('Hatim Benamar', 'hatim.benamar@gmail.com')]
description = "API pour hackathon pack apps"
# The project name
name = "packappsapi"
license = 'Free'

requires_python = ">=3.5"

#liste des tasks https://gist.github.com/miebach/9752025
default_task = "run_unit_tests"
#ATTENTION : pour que 'install' fonctionne, il faut être root pour copier dans le repertoire pip
default_task = ['clean', 'analyze', 'publish', 'install']

@init
def initialize(project):
    project.version = "0.1.0"
    project.build_depends_on('mockito')
    project.build_depends_on('pyassert')



@init
def set_properties (project):
    project.depends_on_requirements("requirements.txt")
    project.build_depends_on_requirements("requirements-dev.txt")


    project.set_property("run_unit_tests_propagate_stdout", True)
    project.set_property("run_unit_tests_propagate_stderr", True)


    analyze_command="rm -f ./src/main/scripts/nbconvert/*.py;" +\
                    "jupyter nbconvert --execute --to=python ./src/main/jupyter/*.ipynb --output-dir=./src/main/scripts/nbconvert;"+\
                    "rm -f ./target/reports/*.html;" +\
                    "jupyter nbconvert --execute --to=html ./src/main/jupyter/*.ipynb --output-dir=./target/reports;" +\
                    "rm -f ./src/main/scripts/*.lprof;"
    project.set_property( "analyze_command", analyze_command)

    project.set_property_if_unset("pytest_coverage_break_build_threshold", 70)
    project.set_property_if_unset("pytest_coverage_html", True)


    project.set_property('pylint_break_build', False)   # Breaks build on any warnings/refactors/convention => too hard
    project.set_property('pylint_score_threshold', 9)
    project.set_property('pylint_include_scripts', True)
    project.set_property('pylint_include_test_sources', True)
    #le pattern est en expression reguliere
    project.set_property('pylint_exclude_patterns', '.*\.csv|.*\.log|.*\.ipynb|.*\.lprof|.*\.pyc')
    project.set_property('pylint_extra_args', ["--max-line-length=250", "--no-docstring-rgx=.*","--min-similarity-lines=50" ])
# , "--jobs=2"
#    project.set_property('pylint_options', ["--max-line-length=200", "--no-docstring-rgx=.*","--min-similarity-lines=50"])

    project.set_property('flake8_max_line_length', 250)
    project.set_property('flake8_include_scripts', True)
    project.set_property('flake8_include_test_sources', True)
    
    # frosted analyse les .log et .csv et pas d'option pour les exclure => ca marche pas bien
    # et comme frosted est deprecated en faveur de flake8, on va pas se prendre la tete
    # project.set_property('frosted_include_test_sources', False)
    # project.set_property('frosted_include_scripts', False)
