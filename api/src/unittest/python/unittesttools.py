# pylint: disable = attribute-defined-outside-init, too-few-public-methods
# pas de definition de __init__ pour les classes de tests sinon  pytest ne fonctionne pas

"""
Classe mère de tous les tests. Regroupement de méthodes
- logging

"""

import logging
from logging.handlers import RotatingFileHandler

import pandas as pd
import numpy as np

class BaseUnitTest(object):

    def setup_method(self, test_method):  # pylint: disable = unused-argument
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter('[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
                                      '%m-%d %H:%M:%S')
        #   formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
        file_handler = RotatingFileHandler('unit_tests.log', 'a', 10 * 1024 * 1024, 5)
        file_handler.setLevel(logging.INFO)
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)

        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(logging.INFO)
        stream_handler.setFormatter(formatter)
        self.logger.addHandler(stream_handler)

