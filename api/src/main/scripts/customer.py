"""
Webservice to access basic ressource Advisor

Apify JpaCustomerRepositoryImpl


"""

import logging
from logging.handlers import RotatingFileHandler

import pymysql

from flask import Flask, request
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from flask_jsonpify import jsonify


logger = logging.getLogger()  # pylint: disable = invalid-name
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s.%(msecs)03d] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',  # pylint: disable = invalid-name
'%m-%d %H:%M:%S')
file_handler = RotatingFileHandler('step_10_resample_ohlcv.log', 'a', 10 * 1024 * 1024, 5)  # pylint: disable = invalid-name
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

stream_handler = logging.StreamHandler()  # pylint: disable = invalid-name
stream_handler.setLevel(logging.INFO)
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

pymysql.install_as_MySQLdb()

db_connect = create_engine('mysql://root:password@mysqldata/containerbank')
app = Flask(__name__)
api = Api(app)

class Customer(Resource):
    def get(self):
        conn = db_connect.connect() # connect to database
        logger.info("Calling customer");
        query = conn.execute("SELECT * FROM customers  ORDER BY last_name, first_name")
        result = {'data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]}
        return jsonify(result)

class CustomerLastName(Resource):

    def get(self, customer_name):
        conn = db_connect.connect()
#        query = conn.execute("select * from customers where customer.last_name LIKE %d  ORDER BY last_name, first_name"  %int(customer_name))
        query = conn.execute("select * from customers where customer.last_name LIKE :lastName  ORDER BY last_name, first_name", lastName=customer_name)
        result = {'data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]}
        return jsonify(result)


#        Query query = this.em.createQuery("SELECT DISTINCT customer FROM Customer customer left join fetch customer.cards WHERE customer.lastName LIKE :lastName");


api.add_resource(Customer, '/v1/customers')
api.add_resource(CustomerLastName, '/v1/customers/<customer_name>')


if __name__ == '__main__':
    logger.info("flask server starting")
    app.run(threaded=True)
