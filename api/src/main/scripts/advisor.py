"""
Webservice to access basic ressource Advisor

Apify JpaAdvisorRepositoryImpl


"""
import os
import logging
from logging.handlers import RotatingFileHandler

import pymysql

from flask import Flask, request
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from flask_jsonpify import jsonify


logger = logging.getLogger()  # pylint: disable = invalid-name
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s.%(msecs)03d] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',  # pylint: disable = invalid-name
'%m-%d %H:%M:%S')
file_handler = RotatingFileHandler('step_10_resample_ohlcv.log', 'a', 10 * 1024 * 1024, 5)  # pylint: disable = invalid-name
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

stream_handler = logging.StreamHandler()  # pylint: disable = invalid-name
stream_handler.setLevel(logging.INFO)
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

pymysql.install_as_MySQLdb()

#db_url = os.environ.get('CB_DATABASE_URL', 'CB_DATABASE_URL must exist')
with open("/etc/secret-volume/dburl", 'r') as dbsecret_file:
    db_url = dbsecret_file.read()

logger.info(db_url)


#db_connect = create_engine('mysql://root:password@mysqldata/containerbank')
db_connect = create_engine(db_url)
app = Flask(__name__)
api = Api(app)

class Advisor(Resource):
    def get(self):
        conn = db_connect.connect() # connect to database
        logger.info("Calling advisor respository");
        query = conn.execute("SELECT * FROM advisors  ORDER BY advisors.last_name, advisors.first_name")
        result = {'data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]}
        return jsonify(result)

class AdvisorSpecialities(Resource):
    def get(self, advisor_id):
        conn = db_connect.connect() # connect to database
        logger.info("Calling advisor specialities respository");
        query = conn.execute("SELECT * FROM specialties, advisor_specialties WHERE specialties.id = advisor_specialties.specialty_id and advisor_id = " + str(advisor_id) + "  ORDER BY specialties.name")
        result = {'data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]}
        return jsonify(result)



api.add_resource(Advisor, '/v1/advisors') # Route_1
api.add_resource(AdvisorSpecialities, '/v1/advisorspecialities/<advisor_id>')

@app.route("/health")
def health():
    return "I'm alive"


if __name__ == '__main__':
    logger.info("flask server starting")
    app.run(threaded=True)
