#!/usr/bin/env bash

#./ci/build.sh

docker build -t api:1.0 .

# docker login --username=iarinnovation

docker tag api:1.0 iarinnovation/api:1.0
docker push  iarinnovation/api:1.0

kubectl delete -f ./docker/k8s/api-advisor-deployment.yaml
kubectl create -f ./docker/k8s/api-advisor-deployment.yaml
