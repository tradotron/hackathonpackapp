package hackathonpackapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/customer")
@RestController
public class CustomerController {
	@Autowired
	CustomerService customerService;

	@RequestMapping(Constants.GET_CUSTOMER_BY_ID)
	public Customer getUserById(@PathVariable Integer customerId) {
		return customerService.getCustomerById(customerId);
	}
	
	@RequestMapping(Constants.GET_ALL_CUSTOMERS)
	public List<Customer> getAllCustomers() {
		return customerService.getAllCustomerss();
	}

	@RequestMapping(value= Constants.SAVE_CUSTOMER, method= RequestMethod.POST)
	public void saveCustomer(@RequestBody Customer customer) {
		customerService.saveCustomer(customer);
	}
}
