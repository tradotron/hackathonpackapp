#!/usr/bin/env bash
#./ci/build.sh

docker build -t data:1.0 .

# docker login --username=iarinnovation
docker tag data:1.0 iarinnovation/data:1.0
docker push iarinnovation/data:1.0

kubectl delete -f ./docker/k8s/mysql-deployment.yaml
kubectl delete -f ./docker/k8s/mysql-pv.yaml

kubectl create -f ./docker/k8s/mysql-pv.yaml
kubectl create -f ./docker/k8s/mysql-deployment.yaml
