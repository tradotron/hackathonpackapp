#!/usr/bin/env bash


kubectl apply -f ./k8s/bank-namespace.yaml
kubectl apply -f ./k8s/bank-configmap.yaml
kubectl apply -f ./k8s/bank-secrets.yaml


kubectl create -f ./data2/docker/k8s/mysql-pv.yaml
kubectl create -f ./data2/docker/k8s/mysql-deployment.yaml
kubectl create -f ./api/docker/k8s/api-advisor-deployment.yaml

kubectl create -f ./portlet/docker/k8s/portlet-advisor-deployment.yaml

kubectl create -f ./containerbank3/docker/k8s/containerbank-deployment.yaml
