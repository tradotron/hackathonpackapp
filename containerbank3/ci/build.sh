#!/usr/bin/env bash

#./ci/build.sh

mvn clean package
#mvn tomcat7:run-war -P=MySQL

docker build -t containerbank:1.0 .

# docker login --username=iarinnovation

docker tag containerbank:1.0 iarinnovation/containerbank:1.0
docker push  iarinnovation/containerbank:1.0

kubectl delete -f ./docker/k8s/containerbank-deployment.yaml
kubectl create -f ./docker/k8s/containerbank-deployment.yaml
