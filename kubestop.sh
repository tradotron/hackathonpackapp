#!/usr/bin/env bash


kubectl delete -f ./containerbank3/docker/k8s/containerbank-deployment.yaml
kubectl delete -f ./portlet/docker/k8s/portlet-advisor-deployment.yaml
kubectl delete -f ./api/docker/k8s/api-advisor-deployment.yaml
kubectl delete -f ./data2/docker/k8s/mysql-deployment.yaml
kubectl delete -f ./data2/docker/k8s/mysql-pv.yaml

