
package org.hackathon.packapp.containerbank.web;

import java.util.Collection;
import java.util.List;
import java.util.ArrayList;

import java.util.Map;

import javax.validation.Valid;

import org.hackathon.packapp.containerbank.model.Advisor;
import org.hackathon.packapp.containerbank.model.Customer;
import org.hackathon.packapp.containerbank.model.CardType;
import org.hackathon.packapp.containerbank.repository.CustomerRepository;
import org.hackathon.packapp.containerbank.repository.jpa.JpaAdvisorRepositoryImpl;
import org.hackathon.packapp.containerbank.repository.jpa.JpaCustomerRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.hackathon.packapp.containerbank.model.Customer;
import org.hackathon.packapp.containerbank.service.BankService;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Wavestone
 */
@Controller
public class CustomerController {

    private static final String VIEWS_customer_CREATE_OR_UPDATE_FORM = "customers/createOrUpdateCustomerForm";
    private final BankService bankService;
    final Logger logger = LoggerFactory.getLogger("CustomerController");



    @RequestMapping(value = { "/customerslastname/{lastname}/customers.json", "/customerslastname/{lastname}/customers.xml"})
    public
    @ResponseBody
    List<CardType> showResourcesCustomerList(@PathVariable("lastname") String lastname) throws DataAccessException{


//        Advisors advisors = new Advisors();
 //       advisors.getAdvisorList().addAll(this.bankService.findAdvisors());
  //      return advisors;

   //     List<Advisor> advisors = new ArrayList<>();


        //List<Customer> customers = new ArrayList<>();
        //customers.addAll(bankService.findCustomerByLastName(lastname));

//        List<Advisor> customers = new ArrayList<>();
//        customers.addAll(bankService.findAdvisors());

        List<CardType> customers = new ArrayList<>();
        customers.addAll(bankService.findCardTypes());

        return customers;


    }
    @RequestMapping(value = { "/customerid/{id}/customer.json", "/customerid/{id}/customer.xml"})
    public
    @ResponseBody
    Customer showResourcesCustomerById(@PathVariable("id") int customerId) throws DataAccessException{
        logger.info("start \n\n\n\n\n\n\n\n\n\n\n\n\n ");
        logger.info("tttttttttttttttttttttttttttttttttt  " + bankService.findCustomerById(customerId).getAddress());
        logger.info("end \n\n\n\n\n\n\n\n\n\n\n\n\n ");

//        return bankService.findCustomerById(customerId);
        Customer cust = bankService.findCustomerById(customerId);
        logger.info("aaaaaaaaaaaaaaaaaaa");

        return cust;

    }
    @RequestMapping(value = { "/customercreate/{address}/{city}/{telephone}/{firstname}/{lastname}", "/customer.xml"})
    public
    @ResponseBody
    Customer showResourcesCustomerCreate(@PathVariable("address") String address,@PathVariable("city") String city,@PathVariable("telephone") String telephone,@PathVariable("firstname") String firstname,@PathVariable("lastname") String lastname ) throws DataAccessException{

        Customer cust = new Customer();
        cust.setAddress(address);
        cust.setCity(city);
        cust.setTelephone(telephone);
        cust.setFirstName(firstname);
        cust.setLastName(lastname);

        bankService.saveCustomer(cust);
        return cust;

    }



    @Autowired
    public CustomerController(BankService bankService) {
        this.bankService = bankService;
    }

    @InitBinder
    public void setAllowedFields(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    @RequestMapping(value = "/customers/new", method = RequestMethod.GET)
    public String initCreationForm(Map<String, Object> model) {
        Customer customer = new Customer();
        model.put("customer", customer);
        return VIEWS_customer_CREATE_OR_UPDATE_FORM;
    }

    @RequestMapping(value = "/customers/new", method = RequestMethod.POST)
    public String processCreationForm(@Valid Customer customer, BindingResult result) {
        if (result.hasErrors()) {
            return VIEWS_customer_CREATE_OR_UPDATE_FORM;
        } else {
            this.bankService.saveCustomer(customer);
            return "redirect:/customers/" + customer.getId();
        }
    }

    @RequestMapping(value = "/customers/find", method = RequestMethod.GET)
    public String initFindForm(Map<String, Object> model) {
        model.put("customer", new Customer());
        return "customers/findCustomers";
    }

    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public String processFindForm(Customer customer, BindingResult result, Map<String, Object> model) {

        // allow parameterless GET request for /customers to return all records
        if (customer.getLastName() == null) {
            customer.setLastName(""); // empty string signifies broadest possible search
        }

        // find customers by last name
        Collection<Customer> results = this.bankService.findCustomerByLastName(customer.getLastName());
        if (results.isEmpty()) {
            // no customers found
            result.rejectValue("lastName", "notFound", "not found");
            return "customers/findCustomers";
        } else if (results.size() == 1) {
            // 1 customer found
            customer = results.iterator().next();
            return "redirect:/customers/" + customer.getId();
        } else {
            // multiple customers found
            model.put("selections", results);
            return "customers/customersList";
        }
    }

    @RequestMapping(value = "/customers/{customerId}/edit", method = RequestMethod.GET)
    public String initUpdateCustomerForm(@PathVariable("customerId") int customerId, Model model) {
        Customer customer = this.bankService.findCustomerById(customerId);
        model.addAttribute(customer);
        return VIEWS_customer_CREATE_OR_UPDATE_FORM;
    }

    @RequestMapping(value = "/customers/{customerId}/edit", method = RequestMethod.POST)
    public String processUpdateCustomerForm(@Valid Customer customer, BindingResult result, @PathVariable("customerId") int customerId) {
        if (result.hasErrors()) {
            return VIEWS_customer_CREATE_OR_UPDATE_FORM;
        } else {
            customer.setId(customerId);
            this.bankService.saveCustomer(customer);
            return "redirect:/customers/{customerId}";
        }
    }

    /**
     * Custom handler for displaying an customer.
     *
     * @param customerId the ID of the customer to display
     * @return a ModelMap with the model attributes for the view
     */
    @RequestMapping("/customers/{customerId}")
    public ModelAndView showCustomer(@PathVariable("customerId") int customerId) {
        ModelAndView mav = new ModelAndView("customers/customerDetails");
        mav.addObject(this.bankService.findCustomerById(customerId));
        return mav;
    }

}
